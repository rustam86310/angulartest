import { Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges, ViewChild, ViewContainerRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DashboardService } from '../services/dashboard.service';
import { ModalService } from '../services/modal.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnChanges {

  totalProducts!: number;
  products: any;
  productsCopy: any;
  isLoggedIn: boolean = false;

  constructor(
    public service: DashboardService,
    private router: Router,
    private modalService: ModalService
  ) { 
    console.log("salkjdasklc");
  }

  // ngOnChanges(changes: SimpleChanges): void {

  //   throw new Error('Method not implemented.');
  // }

  ngOnChanges(changes: SimpleChanges) {
    // Do something when the inputs change
    console.log('Input changes detected:', changes);
  }

  // items = []; // your list of items
  pageSize = 10; // number of items per page
  currentPage = 1; // current page number
  paginationId = 'mypagination'; // unique id for this pagination

  pagedItems(items: any) {
    this.totalProducts = items?.length
    const startIndex = (this.currentPage - 1) * this.pageSize;
    console.log(startIndex);
    console.log(this.pageSize, this.currentPage);
    
    return items.slice(startIndex, startIndex + this.pageSize);
  }

  onPageChange(page: any) {
    console.log("pageNumber", page.pageIndex + 1);
    this.currentPage = page.pageIndex + 1;
   
    let data = sessionStorage.getItem("products");
    if(!data){
      this.service.getAllProducts()
        .subscribe((response: any) => {
          let data = this.pagedItems(response.products);
          this.products = data;
          this.productsCopy = data;

          sessionStorage.setItem("products", JSON.stringify(response.products));
          console.log("response", response);
        });
    } else{
      console.log("response=====", this.products);
      let data1 = this.pagedItems(JSON.parse(data));
      this.products = data1;
      this.productsCopy = data1;
    }
  }
  
  ngOnInit() {
    let data = sessionStorage.getItem("products");
    if(!data){
      this.service.getAllProducts()
        .subscribe((response: any) => {
          let data = this.pagedItems(response.products);
          this.products = data;
          this.productsCopy = data;

          sessionStorage.setItem("products", JSON.stringify(response.products));
          console.log("response", response);
        });
    } else{
      console.log("response=====", data);
      
      let data1 = this.pagedItems(JSON.parse(data));
      this.products = data1;
      this.productsCopy = data1;
    }
  }

  filterProducts(data: any) {
    this.productsCopy = data;
  }

  handleDelete(id: string) {
    this.productsCopy = this.productsCopy.filter((p:any) => p.id !== id);
  }

  confirmationModal(id: string){
    this.modalService.openModal(id).then((confirmed) => {
      if(confirmed) {
        this.handleDelete(confirmed)
      }
    })
  }

  handleClick() {
    console.log("Dddddddddddd");
    
    let data = this.service.setIsLoggedIn(this.isLoggedIn);
    this.isLoggedIn = data;
    console.log("d", this.isLoggedIn, data);
  }

  editForm(data: any) {
    this.router.navigateByUrl('/edit-product', { state: data });
  }
}
