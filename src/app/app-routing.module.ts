import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddFormComponent } from './add-form/add-form.component';
import { BlogComponent } from './blog/blog.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account/account.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'edit-product', component: AddFormComponent },
  { path: 'add-product', component: AddFormComponent },
  { path: 'account', component: AccountComponent },
  { path: '**', redirectTo: 'login', pathMatch: "full" },
  { path: 'login', component: LoginComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
