import { Component } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent {
  selectedTab = 0;
  constructor(private meta: Meta) { }


  ngOnInit() {
    this.meta.addTag({ name: 'description', content: 'Thissdfsd doifgljspodgijis the description for my component' });
    this.meta.addTag({ property: 'og:title', content: 'My dsfsdfsdComponent' });
    // Add more meta tags as needed
  }

  handleTabChange(event: any) {
    this.selectedTab = event.index;
    console.log(`Tab ${event.index + 1} selected`);
  }
}
