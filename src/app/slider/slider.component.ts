import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
	@Input() productList!: any
  searchKeyword: string = ''
  categoryArray: any = []
  servicesData!: boolean
  @Output() data = new EventEmitter();

  constructor(
    private services: DashboardService
  ){}

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    let k = this.productList.filter((item: any, index: any) => this.productList.findIndex((e: any) => item.category == e.category) === index)
    console.log("categoryArray", k);
    this.categoryArray = k;
    this.services.dataUpdated.subscribe((data: boolean) => {
      console.log("data====", data);
      
      this.servicesData = data;
    });
  }

  changeWebsite(e: any) {
    console.warn(e.target.value);
		this.data.emit(this.productList.filter((p: any) => p.category === e.target.value))
  }

  handleSearch() {
    console.warn("lj", this.searchKeyword);
		this.data.emit(this.productList.filter((p: any) => p.brand.toString().toLowerCase().includes(this.searchKeyword) || p.title.toString().toLowerCase().includes(this.searchKeyword) || p.category.toString().toLowerCase().includes(this.searchKeyword)))
  }
  handleSearchChange(e: any){
    e = e.toLowerCase();
    this.searchKeyword = e;
		this.data.emit(this.productList.filter((p: any) => p.brand.toString().toLowerCase().match(e) || p.title.toString().toLowerCase().match(e) || p.category.toString().toLowerCase().match(e)))
  }

}