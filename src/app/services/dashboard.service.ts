import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
  
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private url = 'https://dummyjson.com/products';
  dataUpdated = new EventEmitter<boolean>();
  constructor(private httpClient: HttpClient) { }
  
  getAllProducts(){
    return this.httpClient.get(this.url);
  }

  getProductById(id: string){
    return this.httpClient.get(this.url + '/' + id);
  }

  async deleteProduct(id: string){
    await this.httpClient.delete(this.url + '/' + id);
    console.log("deleted");
  }

  postProduct(data: any){
    return this.httpClient.post(this.url, data);
  }

  openPopup(displayStyle: any) {
    return displayStyle = "block";
  }
  closePopup(displayStyle: any) {
    return displayStyle = "none";
  }

  setIsLoggedIn(isLoggedIn: any) {
    console.log("setIsLoggedIn", isLoggedIn);
    
    this.dataUpdated.emit(!isLoggedIn);
    return !isLoggedIn;
  }
  
}