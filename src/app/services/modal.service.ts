import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modals/modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private modalService: NgbModal) { }

  openModal(id: string) {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.productId = id;
    return modalRef.result;
  }

}