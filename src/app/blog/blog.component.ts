import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Country } from '../interfaces/common';
// import { DecimalPipe, NgFor, NgIf, NgSwitch } from '@angular/common';
@Component({
	selector: 'app-blog',
	templateUrl: './blog.component.html',
	styleUrls: ['./blog.component.css']
})

// ✔✔✔✔
export class BlogComponent implements OnInit {
	
	@Input() blog!: string
	@Output() data = new EventEmitter();
	rowId: number = 0
	countries: Country[] = [
		{
			name: 'Russia',
			flag: 'f/f3/Flag_of_Russia.svg',
			area: 17075200,
			population: 146989754,
		},
		{
			name: 'Canada',
			flag: 'c/cf/Flag_of_Canada.svg',
			area: 9976140,
			population: 36624199,
		},
		{
			name: 'United States',
			flag: 'a/a4/Flag_of_the_United_States.svg',
			area: 9629091,
			population: 324459463,
		},
		{
			name: 'China',
			flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
			area: 9596960,
			population: 1409517397,
		},
	];

	ngOnInit(): void {
		console.log("countries", this.countries);
		console.log(this.blog)
	}
	addNewItem(country: Country) {
		this.rowId = country.area;
		console.log("event", this.rowId);
		this.data.emit(country)
	}
}







