export interface Country {
	name: string;
	flag: string;
	area: number;
	population: number;
}
export interface Product {
	title: string,
	brand: string,
	category: string,
	price: number | string
}