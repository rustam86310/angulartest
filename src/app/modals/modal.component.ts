import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from '../interfaces/common';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})

export class ModalComponent implements OnInit {
  productId: any; 
  constructor(
    private dashboardService: DashboardService,
    private activeModal: NgbActiveModal
    ) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
  }

  handleDeleteConfirm() {
    this.activeModal.close(this.productId);
  }

  handleDecline() {
    this.activeModal.dismiss()
  }


}


// import { Component } from '@angular/core';
// import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

// @Component({
//   selector: 'app-modal',
//   template: `
//     <div class="modal-header">
//       <h4 class="modal-title">Modal Title</h4>
//       <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
//         <span aria-hidden="true">&times;</span>
//       </button>
//     </div>
//     <div class="modal-body">
//       Modal content goes here.
//     </div>
//     <div class="modal-footer">
//       <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss()">Close</button>
//       <button type="button" class="btn btn-primary" (click)="activeModal.close()">Save changes</button>
//     </div>
//   `
// })
// export class ModalComponent {

//   constructor(public activeModal: NgbActiveModal) { }

// }