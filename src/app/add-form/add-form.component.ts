import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
// import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { Product } from '../interfaces/common';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private toastr: ToastrService
  ) {
  }

  checkoutForm = this.fb.group({
    title: null,
    brand: null,
    category: null,
    price: null
  });

  errorObj: Product | any = {
    title: "",
    brand: "",
    category: "",
    price: ""
  }

  product: Product = {} as Product

  ngOnInit(): void {
    // this.route.queryParams
    //   .subscribe(params => {
    //     this.id = params['id'];
    //   }
    //   );

    let temp: Product | any = this.location.getState();

    console.log("temp", temp);
    if(!temp.title) return
    this.checkoutForm.patchValue({
      title: temp.title,
      brand: temp.brand,
      category: temp.category,
      price: temp.price
    });
    this.product = temp
    console.log("temp", temp);
    // if (this.id) {
    //   this.service.getProductById(this.id)
    //     .subscribe((response: any) => {
    //       console.log("response", response);
    //       this.product = response;
    //       this.checkoutForm.patchValue({
    //         title: response.title,
    //         brand: response.brand,
    //         category: response.category,
    //         price: response.price
    //       });
    //     });
    // }

    // throw new Error('Method not implemented.');
  }

  onSubmit(e: any): void {
    let flag = false;
    if (!this.checkoutForm.value.title) {
      flag = true;
      this.errorObj.title = 'Title is required';
    }
    if (!this.checkoutForm.value.brand) {
      flag = true;
      this.errorObj.brand = 'Brand is required';
    }
    if (!this.checkoutForm.value.category) {
      flag = true;
      this.errorObj.category = 'Category is required';
    }
    if (!this.checkoutForm.value.price) {
      flag = true;
      this.errorObj.price = 'Price is required';
    }
    
    if(flag){
      return
    }

    console.warn('Your order has been submitted', this.checkoutForm.value);
    this.toastr.success('Product added successfully.', 'Product Add!', {
      timeOut: 3000,
    });

    this.checkoutForm.reset();
    this.errorObj = {
      title: "",
      brand: "",
      category: "",
      price: ""
    }
  }

  handleChange(value: string, name: string) {
    if (!value) {
      this.errorObj[name] = name.slice(0, 1).toUpperCase() + name.slice(1, name.length) + ' is required';
    } else {
      this.errorObj[name] = "";
    }
  }

}
